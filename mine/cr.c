static void logger_checksum_add_byte(uint8_t s[2], uint8_t byte)
{
        uint8_t t1, t2; 
        t1 = s[1];
        s[1] = s[0];
        t2 = (s[0] << 1) | ((s[0] & 0x80) >> 7);
        s[0] = t2 + t1 + byte; 
}
