#include <inttypes.h>

static void checksum_add_byte(uint8_t s[2], uint8_t byte)
{
	uint8_t t1, t2; 

	t1 = s[1];
	s[1] = s[0];
	t2 = (s[0] << 1) | ((s[0] & 0x80) >> 7);
	s[0] = t2 + t1 + byte; 
}

int main()
{
	int i;
	uint8_t s[2] = { 0xAA, 0xAA };

	for(i = 0; i < 1000000000; i++)
		checksum_add_byte(s, '?');

	printf("%02x%02x\n", (int)s[0], (int)s[1]);
}
