section .data

nums	db	'0123456789ABCDEF'
asdf	db	0x123456

;section .bss
;outbuf	resb	64

section .text

	BITS 16

    global _start

_start:

	mov eax,asdf
	call print
	

	mov	ebx,0
        mov     eax,1
        int     0x80

print: ; uses eax
	push ebx
	push ecx
	push edx

print_loop:

	mov ebx,nums
	mov edx,eax
	and edx,0xf
	mov ecx,edx
	add ebx,ecx

	push eax

        mov     edx,1
        mov     ecx,ebx
        mov     ebx,1
        mov     eax,4
        int     0x80

	pop eax

	add eax,1
	cmp eax,0
	jnz print_loop

	pop edx
	pop ecx
	pop ebx

	ret
